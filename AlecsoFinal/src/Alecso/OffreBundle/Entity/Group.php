<?php

namespace Alecso\OffreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Group
 *
 * @ORM\Table(name="group", indexes={@ORM\Index(name="fk_group_users1_idx", columns={"id_user"})})
 * @ORM\Entity
 */
class Group
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_group", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idGroup;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id_user")
     * })
     */
    private $idUser;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="User", inversedBy="idGugroup")
     * @ORM\JoinTable(name="group_user",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_GUgroup", referencedColumnName="id_group")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_GUuser", referencedColumnName="id_user")
     *   }
     * )
     */
    private $idGuuser;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idGuuser = new \Doctrine\Common\Collections\ArrayCollection();
    }

}

