<?php

namespace Alecso\OffreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Compitiondemande
 *
 * @ORM\Table(name="compitiondemande", indexes={@ORM\Index(name="fk_compitiondemande_competition1_idx", columns={"idcmpt"}), @ORM\Index(name="fk_compitiondemande_user1_idx", columns={"iduser"})})
 * @ORM\Entity
 */
class Compitiondemande
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="admindecision", type="string", length=255, nullable=true)
     */
    private $admindecision;

    /**
     * @var \Competition
     *
     * @ORM\ManyToOne(targetEntity="Competition")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcmpt", referencedColumnName="id_cmpt")
     * })
     */
    private $idcmpt;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="iduser", referencedColumnName="id_user")
     * })
     */
    private $iduser;


}

