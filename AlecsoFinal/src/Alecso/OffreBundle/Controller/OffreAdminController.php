<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 15/04/2019
 * Time: 22:36
 */

namespace Alecso\OffreBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Alecso\OffreBundle\Entity\Offre;
use Alecso\OffreBundle\Entity\User;
use Alecso\OffreBundle\Entity\Group;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Response;



class OffreAdminController extends  Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */

    public function indexAction(Request $request)
    {
        $requestUsername = $request->get('username');
        $requestPassword = $request->get('password');

        $em = $this->getDoctrine()->getManager();
        $login=$em->getRepository('AlecsoOffreBundle:User')->findBy(['username'=>$requestUsername]);

        if(is_null($login)){
            return new Response(json_encode("NOPE"));
        }
        $encoder_service = $this->get('security.encoder_factory');
        $encoder = $encoder_service->getEncoder($login);
        $bool = $encoder->isPasswordValid($login->getPassword(),$requestPassword,$login->getSalt());
        if($bool == 1){
            //if(!$user->hasRole('ROLE_ADMIN')){
            $result['id'] = $login->getId();
            $result['username'] = $login->getUsername();
            $result['email'] = $login->getEmail();
            return new Response(json_encode($result));
            //}else{
            // return new Response(json_encode("NOPE"));
            // }
        }else{
            return new Response(json_encode("NOPE"));
        }
        //echo $requestUsername." - ".$requestPassword;
        //die();
        //$result['entities']['error'] = $requestUsername." - ".$requestPassword;
    }
    public function registerAction(Request $request)
    {
        $user = new User();
        $em = $this->getDoctrine()->getManager();
        $requestUsername = $request->get('username');
        $requestPassword = $request->get('password');
        $requestEmail = $request->get('email');

        $user->setEmail($requestEmail);
        $user->setUsername($requestUsername);

        $encoder_service = $this->get('security.encoder_factory');
        $encoder = $encoder_service->getEncoder($user);
        $encoded = $encoder->encodePassword($user, $requestPassword);

        // $user = $em->getRepository('AppBundle:User')->findOneBy([ 'username' => $requestUsername ]);
        /*
                if(!is_null($user)){
                    echo $user->getEmail();
                }else{
                    echo 'Not Exist';
                }

                die();
        */
        $user->setPassword($encoded);
        $em->persist($user);
        $em->flush($user);


        $user = $em->getRepository('AlecsoOffreBundle:User')->findOneBy([ 'username' => $requestUsername ]);

        return new Response(json_encode("OKAY"));
    }



    public function allAction(){
    $offres = $this->getDoctrine()->getManager()
        ->getRepository('AlecsoOffreBundle:Offre')
        ->findAll();
    $serializer = new Serializer([new ObjectNormalizer()]);
    $formatted = $serializer->normalize($offres);
    return new JsonResponse($formatted);
}
    public function getOffreAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $offre = $em->getRepository('AlecsoOffreBundle:Offre')->find($id);
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($offre);
        return new JsonResponse($formatted);
    }
    public function addOffreAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $offre = new Offre();
        $offre->setTitle($request->get('title'));
        $offre->setDescription($request->get('description'));
        $offre->setType($request->get('type'));
        $offre->setMedia($request->get('media'));
        $offre->setAdresse($request->get('adresse'));
        $offre->setVille($request->get('ville'));
        $offre->setCategorie($request->get('categorie'));
        $offre->setCodePost($request->get('code_post'));
        $offre->setDateCreate($request->get('date_create'));
        $offre->setDateFin($request->get('date_fin'));
        $em->persist($offre);
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($offre);
        return new JsonResponse($formatted);
    }
    public function deleteOffreAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $Offre = $em->getRepository('AlecsoOffreBundle:Offre')->find($id);
        $Offre->setIdAdmin(null);
        $em->remove($Offre);
        $em->flush();
        $this->addFlash('message','offre supprimé');
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($Offre);
        return new JsonResponse($formatted);
    }
    public function updateOffreAction($id,Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $offre=$em ->getRepository('AlecsoOffreBundle:Offre')->find($id);

        $offre->setTitle($request->get('title'));
        $offre->setDescription($request->get('description'));
        $offre->setType($request->get('type'));
        $offre->setMedia($request->get('media'));
        $offre->setAdresse($request->get('adresse'));
        $offre->setVille($request->get('ville'));
        $offre->setCategorie($request->get('categorie'));
        $offre->setCodePost($request->get('code_post'));
        $offre->setDateCreate($request->get('date_create'));
        $offre->setDateFin($request->get('date_fin'));
        $em->persist($offre);
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($offre);
        return new JsonResponse($formatted);
    }

    /**
     * List Des Offres
     *
     */
    public function viewAction()
    {
        $offres = $this->getDoctrine()->getRepository('AlecsoOffreBundle:Offre')->findAll();
        return $this->render('@AlecsoOffre/Admin/viewOffre.html.twig',['offres' => $offres]);
    }

    /**
     * Ajouter Un Offre .
     *
     */

    public function ajouteAction(Request $request)
    {
        $offre = new Offre();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $form = $this->createFormBuilder($offre)
            ->add('title', TextType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 255
                ),'required' => true))

            ->add('description', TextareaType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 2550
                ),'required' => true))
            ->add('type', ChoiceType::class, [
                'choices'  => [
                    'A plein temps' => 1,
                    'Prestataire' => 2,
                    'Stage' => 3,
                    'A temps partiel' => 4,
                ],'label' => false,
            ])
            ->add('categorie', ChoiceType::class, [
                'choices'  => [
                    'Informatique' => 'Informatique',
                    'Administration et travail de bureau' =>  'Administration et travail de bureau',
                    'Publicité et marketing' => 'Publicité et marketing',
                    'Art, mode et design' => 'Art, mode et design',
                ],'label' => false,
            ])
            ->add('date_create', DateTimeType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 5
                ),'required' => true))
            ->add('date_fin', DateTimeType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 5
                ),'required' => true))
            ->add('ville', ChoiceType::class, [
                'choices'  => [
                    'Algérie' => 'Algérie',
                    'Bahreïn' => 'Bahreïn',
                    'Comores' => 'Comores',

                    'Djibouti' => 'Djibouti',
                    'Égypte' => 'Égypte',
                    'Iraq' => 'Iraq',

                    'Jordanie' => 'Jordanie',
                    'Koweït' => 'Koweït',
                    'Liban' => 'Liban',

                    'Libye' => 'Libye',
                    'Mauritanie' => 'Mauritanie',
                    'Maroc' => 'Maroc',

                    'Oman' => 'Oman',
                    'Palestine' => 'Palestine',
                    'Qatar' => 'Qatar',

                    'Arabie saoudite' => 'Arabie saoudite',
                    'Somalie' => 'Somalie',
                    'Soudan' => 'Soudan',

                    'Syrie' => 'Syrie',
                    'Tunisie' => 'Tunisie',
                    'Émirats arabes unis' => 'Émirats arabes unis',

                    'Yémen' => 'Yémen',
                ],'label' => false, 'attr' =>
                    array(
                        'class' => 'form-control'
                    )
            ])
            ->add('adresse', TextType::class , array('label' => false,'attr' =>
                array(
                    //'maxlength' => 5
                ),'required' => true))
            ->add('code_post', TextType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 5
                ),'required' => true))

            ->add('media', FileType::class , array('label' => false,'attr' =>
                array(
                    'accept' => ".jpg,.jpeg,.png"
                )))
            ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $file = $request->files->get('form')['media'];
            $uploads_folder = $this->getParameter('uploads_directory');
            $filename =md5(uniqid()). '.' . $file->guessExtension();
            $file->move(
                $uploads_folder,
                $filename
            );
            $offre->setMedia($filename);
            $em = $this->getDoctrine()->getManager();

            $admin = $this->getDoctrine()->getRepository('AlecsoOffreBundle:Admin')->find($user);
            $offre->setIdAdmin($admin);
            $em->persist($offre);
            $em->flush($offre);
            return $this->redirectToRoute('alecso_offre_view_admin');
        }
        return $this->render('@AlecsoOffre/Admin/ajoutOffre.html.twig', array(
            'form' => $form->CreateView(),
            'offre' => $offre
        ));
    }
    /**
     * Modifier Un Offre .
     *
     */
    public function updateAction($id,Request $request)
    {
        $Offre = $this->getDoctrine()->getRepository('AlecsoOffreBundle:Offre')->find($id);
        $img = $Offre->getMedia();
        $Offre->setTitle($Offre->getTitle());
        $Offre->setType($Offre->getType());
        $Offre->setDescription($Offre->getDescription());

        $Offre->setDateCreate($Offre->getDateCreate());
        $Offre->setDateFin($Offre->getDateFin());

        $Offre->setVille($Offre->getVille());
        $Offre->setAdresse($Offre->getAdresse());
        $Offre->setCodePost($Offre->getCodePost());




        $form = $this->createFormBuilder($Offre)
            ->add('title', TextType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 250
                ),'required' => true))
            ->add('type', ChoiceType::class, [
                'choices'  => [
                    'A plein temps' => 1,
                    'Prestataire' => 2,
                    'Stage' => 3,
                    'A temps partiel' => 4,
                ],'label' => false,
            ])
            ->add('categorie', ChoiceType::class, [
                'choices'  => [
                    'Informatique' => 'Informatique',
                    'Administration et travail de bureau' =>  'Administration et travail de bureau',
                    'Publicité et marketing' => 'Publicité et marketing',
                    'Art, mode et design' => 'Art, mode et design',
                ],'label' => false,
            ])
            ->add('description', TextareaType::class , array('label' => false,'attr' =>
                array(

                ),'required' => true))
            ->add('date_create', DateTimeType::class , array('label' => false,'attr' =>
                array(

                ),'required' => true))
            ->add('date_fin', DateTimeType::class , array('label' => false,'attr' =>
                array(

                ),'required' => true))
            ->add('ville', ChoiceType::class, [
                'choices'  => [
                    'Algérie' => 'Algérie',
                    'Bahreïn' => 'Bahreïn',
                    'Comores' => 'Comores',

                    'Djibouti' => 'Djibouti',
                    'Égypte' => 'Égypte',
                    'Iraq' => 'Iraq',

                    'Jordanie' => 'Jordanie',
                    'Koweït' => 'Koweït',
                    'Liban' => 'Liban',

                    'Libye' => 'Libye',
                    'Mauritanie' => 'Mauritanie',
                    'Maroc' => 'Maroc',

                    'Oman' => 'Oman',
                    'Palestine' => 'Palestine',
                    'Qatar' => 'Qatar',

                    'Arabie saoudite' => 'Arabie saoudite',
                    'Somalie' => 'Somalie',
                    'Soudan' => 'Soudan',

                    'Syrie' => 'Syrie',
                    'Tunisie' => 'Tunisie',
                    'Émirats arabes unis' => 'Émirats arabes unis',

                    'Yémen' => 'Yémen',
                ],'label' => false
            ])
            ->add('adresse', TextType::class , array('label' => false,'attr' =>
                array(
                    //'maxlength' => 5
                ),'required' => true))
            ->add('code_post', TextType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 8
                ),'required' => true))

            ->add('media', FileType::class , array('label' => false,'attr' =>
                array(
                    'accept' => ".jpg,.jpeg,.png"
                ), 'required' => false,'data_class' => null))
            ->getForm();

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $file = $request->files->get('form')['media'];
            if($form->get('media')->getData() == null){
                $Offre->setMedia($img);
            }else{
                $uploads_folder = $this->getParameter('uploads_directory');
                $filename =md5(uniqid()). '.' . $file->guessExtension();
                $file->move(
                    $uploads_folder,
                    $filename
                );
                $Offre->setMedia($filename);
            }
            $em = $this->getDoctrine()->getManager();
            $Offre = $em->getRepository('AlecsoOffreBundle:Offre')->find($id);

            $em->flush();
            return $this->redirectToRoute('alecso_offre_view_admin');
        }

        return $this->render('@AlecsoOffre/Admin/updateOffre.html.twig',[
            'form' => $form->CreateView()
        ]);
    }
    /**
     * Supprimer Un Offre .
     *
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $Offre = $em->getRepository('AlecsoOffreBundle:Offre')->find($id);
        $Offre->setIdAdmin(null);
        $em->remove($Offre);
        $em->flush();
        $this->addFlash('message','offre supprimé');
        return $this->redirectToRoute('alecso_offre_view_admin');
    }
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $Offre = $em->getRepository('AlecsoOffreBundle:Offre')->find($id);

        if($Offre->getIdAdmin() == null){
            $partenaire = $Offre->getIdPartenaire();
        }else{
            $partenaire = $em->getRepository('AlecsoOffreBundle:Partenaire')->findOneBy([ 'idUser' => null ]);
        }
        return $this->render('@AlecsoOffre/Admin/showOffre.html.twig',[
            'offre' => $Offre,
            'partenaire' => $partenaire
        ]);
    }
}