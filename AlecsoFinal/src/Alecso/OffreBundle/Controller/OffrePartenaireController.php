<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 16/04/2019
 * Time: 21:30
 */

namespace Alecso\OffreBundle\Controller;

use Alecso\EvenementBundle\Entity\Evenement;
use Alecso\OffreBundle\Entity\Offre;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class OffrePartenaireController extends  Controller
{


    public function allAction(){
        $offres = $this->getDoctrine()->getManager()
            ->getRepository('AlecsoOffreBundle:Offre')
            ->findAll();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($offres);
        return new JsonResponse($formatted);
    }

    public function getOffreAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $offre = $em->getRepository('AlecsoOffreBundle:Offre')->find($id);
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($offre);
        return new JsonResponse($formatted);
    }

    public function addOffreAction(Request $request) {
        echo($request);
        $em = $this->getDoctrine()->getManager();
        $offre = new Offre();
        $offre->setTitle($request->get('title'));
        $offre->setDescription($request->get('description'));
        $offre->setType($request->get('type'));
        $offre->setMedia($request->get('media'));
        $offre->setAdresse($request->get('adresse'));
        $offre->setVille($request->get('ville'));
        $offre->setCategorie($request->get('categorie'));
        $offre->setCodePost($request->get('code_post'));
        $offre->setDateCreate($request->get('date_create'));
        $offre->setDateFin($request->get('date_fin'));
        $em->persist($offre);
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($offre);
        return new JsonResponse($formatted);
    }
    public function deleteOffreAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $Offre = $em->getRepository('AlecsoOffreBundle:Offre')->find($id);
        $Offre->setIdAdmin(null);
        $em->remove($Offre);
        $em->flush();
        $this->addFlash('message','offre supprimé');
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($Offre);
        return new JsonResponse($formatted);
    }
    public function updateOffreAction($id,Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $offre=$em ->getRepository('AlecsoOffreBundle:Offre')->find($id);

        $offre->setTitle($request->get('title'));
        $offre->setDescription($request->get('description'));
        $offre->setType($request->get('type'));
        $offre->setMedia($request->get('media'));
        $offre->setAdresse($request->get('adresse'));
        $offre->setVille($request->get('ville'));
        $offre->setCategorie($request->get('categorie'));
        $offre->setCodePost($request->get('code_post'));
        $offre->setDateCreate($request->get('date_create'));
        $offre->setDateFin($request->get('date_fin'));
        $em->persist($offre);
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($offre);
        return new JsonResponse($formatted);
    }

    /**
     * List Des Evenements
     *
     */
    public function viewAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $partenaire = $em->getRepository('AlecsoOffreBundle:Partenaire')->findOneBy([ 'idUser' => $user]);
        dump($partenaire);
        $result2 = $em->getRepository('AlecsoOffreBundle:Offre')
            ->createQueryBuilder('e')
            ->leftJoin('e.idPartenaire', 'p')
            ->where('p.idPartenaire = :id')
            ->setParameter('id', $partenaire->getIdPartenaire())
            ->getQuery()
            ->getResult();
        return $this->render('@AlecsoOffre/Partenaire/viewOffre.html.twig',['offres' => $result2]);
    }
    /**
     * Ajoute Un Offre .
     *
     */
    public function ajouteAction(Request $request)
    {

        $offre = new Offre();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $form = $this->createFormBuilder($offre)
            ->add('title', TextType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 250
                ),'required' => true))
            ->add('type', ChoiceType::class, [
                'choices'  => [
                    'A plein temps' => 1,
                    'Prestataire' => 2,
                    'Stage' => 3,
                    'A temps partiel' => 4,
                ],'label' => false,
            ])
            ->add('description', TextareaType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 2550
                ),'required' => true))
            ->add('categorie', TextareaType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 255
                ),'required' => true))
            ->add('categorie', ChoiceType::class, [
                'choices'  => [
                    'Informatique' => 'Informatique',
                    'Administration et travail de bureau' =>  'Administration et travail de bureau',
                    'Publicité et marketing' => 'Publicité et marketing',
                    'Art, mode et design' => 'Art, mode et design',
                ],'label' => false,
            ])
            ->add('date_create', DateTimeType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 5
                ),'required' => true))
            ->add('date_fin', DateTimeType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 5
                ),'required' => true))
            ->add('ville', ChoiceType::class, [
                'choices'  => [
                    'Algérie' => 'Algérie',
                    'Bahreïn' => 'Bahreïn',
                    'Comores' => 'Comores',

                    'Djibouti' => 'Djibouti',
                    'Égypte' => 'Égypte',
                    'Iraq' => 'Iraq',

                    'Jordanie' => 'Jordanie',
                    'Koweït' => 'Koweït',
                    'Liban' => 'Liban',

                    'Libye' => 'Libye',
                    'Mauritanie' => 'Mauritanie',
                    'Maroc' => 'Maroc',

                    'Oman' => 'Oman',
                    'Palestine' => 'Palestine',
                    'Qatar' => 'Qatar',

                    'Arabie saoudite' => 'Arabie saoudite',
                    'Somalie' => 'Somalie',
                    'Soudan' => 'Soudan',

                    'Syrie' => 'Syrie',
                    'Tunisie' => 'Tunisie',
                    'Émirats arabes unis' => 'Émirats arabes unis',

                    'Yémen' => 'Yémen',
                ],'label' => false, 'attr' =>
                    array(
                        'class' => 'form-control'
                    )
            ])
            ->add('adresse', TextType::class , array('label' => false,'attr' =>
                array(
                    //'maxlength' => 255
                ),'required' => true))
            ->add('code_post', TextType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 5
                ),'required' => true))

            ->add('media', FileType::class , array('label' => false,'attr' =>
                array(
                    'accept' => ".jpg,.jpeg,.png"
                )))
            ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $file = $request->files->get('form')['media'];
            $uploads_folder = $this->getParameter('uploads_directory');
            $filename =md5(uniqid()). '.' . $file->guessExtension();
            $file->move(
                $uploads_folder,
                $filename
            );
            $offre->setMedia($filename);
            $em = $this->getDoctrine()->getManager();
            $partenaire = $em->getRepository('AlecsoOffreBundle:Partenaire')->findOneBy([ 'idUser' => $user ]);

            $offre->setIdPartenaire($partenaire);

            $em->persist($offre);
            $em->flush($offre);
            $em->flush($partenaire);
            return $this->redirectToRoute('alecso_offre_view_partenaire');
        }
        return $this->render('@AlecsoOffre/Partenaire/ajoutOffre.html.twig', array(
            'form' => $form->CreateView(),
            'offret' => $offre
        ));
    }
    /**
     * Supprimer un Offre .
     *
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $Offre = $em->getRepository('AlecsoOffreBundle:Offre')->find($id);
        $Offre->setIdAdmin(null);
        $em->remove($Offre);
        $em->flush();
        $this->addFlash('message','offre supprimé');
        return $this->redirectToRoute('alecso_offre_view_partenaire');
    }
    /**
     * Modifier un Offre .
     *
     */
    public function updateAction($id,Request $request)
    {

        $offre = $this->getDoctrine()->getRepository('AlecsoOffreBundle:Offre')->find($id);
        $img = $offre->getMedia();
        $offre->setTitle($offre->getTitle());
        $offre->setType($offre->getType());
        $offre->setDescription($offre->getDescription());

        $offre->setDateCreate($offre->getDateCreate());
        $offre->setDateFin($offre->getDateFin());

        $offre->setVille($offre->getVille());
        $offre->setAdresse($offre->getAdresse());
        $offre->setCodePost($offre->getCodePost());



        $form = $this->createFormBuilder($offre)
            ->add('title', TextType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 250
                ),'required' => true))
            ->add('type', ChoiceType::class, [
                'choices'  => [
                    'A plein temps' => 1,
                    'Prestataire' => 2,
                    'Stage' => 3,
                    'A temps partiel' => 4,
                ],'label' => false,
            ])
            ->add('categorie', ChoiceType::class, [
                'choices'  => [
                    'Informatique' => 'Informatique',
                    'Administration et travail de bureau' =>  'Administration et travail de bureau',
                    'Publicité et marketing' => 'Publicité et marketing',
                    'Art, mode et design' => 'Art, mode et design',
                ],'label' => false,
            ])
            ->add('description', TextareaType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 2550
                ),'required' => true))
            ->add('date_create', DateTimeType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 10
                ),'required' => true))
            ->add('date_fin', DateTimeType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 10
                ),'required' => true))
            ->add('ville', ChoiceType::class, [
                'choices'  => [
                    'Algérie' => 'Algérie',
                    'Bahreïn' => 'Bahreïn',
                    'Comores' => 'Comores',

                    'Djibouti' => 'Djibouti',
                    'Égypte' => 'Égypte',
                    'Iraq' => 'Iraq',

                    'Jordanie' => 'Jordanie',
                    'Koweït' => 'Koweït',
                    'Liban' => 'Liban',

                    'Libye' => 'Libye',
                    'Mauritanie' => 'Mauritanie',
                    'Maroc' => 'Maroc',

                    'Oman' => 'Oman',
                    'Palestine' => 'Palestine',
                    'Qatar' => 'Qatar',

                    'Arabie saoudite' => 'Arabie saoudite',
                    'Somalie' => 'Somalie',
                    'Soudan' => 'Soudan',

                    'Syrie' => 'Syrie',
                    'Tunisie' => 'Tunisie',
                    'Émirats arabes unis' => 'Émirats arabes unis',

                    'Yémen' => 'Yémen',
                ],'label' => false
            ])
            ->add('adresse', TextType::class , array('label' => false,'attr' =>
                array(
                    //'maxlength' => 255
                ),'required' => true))
            ->add('code_post', TextType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 8
                ),'required' => true))

            ->add('media', FileType::class , array('label' => false,'attr' =>
                array(
                    'accept' => ".jpg,.jpeg,.png"
                ), 'required' => false,'data_class' => null))
            ->getForm();

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $file = $request->files->get('form')['media'];
            if($form->get('media')->getData() == null){
                $offre->setMedia($img);
            }else{
                $uploads_folder = $this->getParameter('uploads_directory');
                $filename =md5(uniqid()). '.' . $file->guessExtension();
                $file->move(
                    $uploads_folder,
                    $filename
                );
                $offre->setMedia($filename);
            }
            $em = $this->getDoctrine()->getManager();
            $offre = $em->getRepository('AlecsoOffreBundle:Offre')->find($id);

            $em->flush();
            return $this->redirectToRoute('alecso_offre_view_partenaire');
        }

        return $this->render('@AlecsoOffre/Partenaire/updateOffre.html.twig',[
            'form' => $form->CreateView()
        ]);
    }
    /**
     * Afficher un Offre .
     *
     */
    public function showOffreAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $offre = $em->getRepository('AlecsoOffreBundle:Offre')->find($id);

        if($offre->getIdAdmin() == null){
            $partenaire = $offre->getIdPartenaire();
        }else{
            $partenaire = $em->getRepository('AlecsoOffreBundle:Partenaire')->findOneBy([ 'idUser' => null ]);
        }
        return $this->render('@AlecsoOffre/Partenaire/showOffre.html.twig',[
            'offre' => $offre,
            'partenaire' => $partenaire
        ]);
    }


}