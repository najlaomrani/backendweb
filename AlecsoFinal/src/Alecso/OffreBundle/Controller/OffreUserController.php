<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 17/04/2019
 * Time: 07:21
 */

namespace Alecso\OffreBundle\Controller;

use Alecso\OffreBundle\Entity\Cvs;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Constraints\DateTime;

class OffreUserController extends Controller
{
    public function allOffreAction()
    {
        $offres = $this->getDoctrine()->getManager()->getRepository('AlecsoOffreBundle:Offre')->findAll();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($offres);
        return new JsonResponse($formatted);

    }
    public function getOffreAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $offre = $em->getRepository('AlecsoOffreBundle:Offre')->find($id);
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($offre);
        return new JsonResponse($formatted);
    }
    public function allCvAction()
    {
        $cvs = $this->getDoctrine()->getManager()->getRepository('AlecsoOffreBundle:Cvs')->findAll();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($cvs);
        return new JsonResponse($formatted);

    }
    public function getCvAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $cv = $em->getRepository('AlecsoOffreBundle:Cvs')->find($id);
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($cv);
        return new JsonResponse($formatted);
    }

    public function deletCvAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $cv = $em->getRepository('AlecsoOffreBundle:Cvs')->find($id);
        $cv->setIdUser(null);
        $em->remove($cv);
        $em->flush();
        $this->addFlash('message','offre supprimé');
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($cv);
        return new JsonResponse($formatted);
    }

    public function addCvAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $cv = new Cvs();
        $cv->setDiplome($request->get('diplome'));
        $cv->setDescription($request->get('description'));
        $cv->setPhoto($request->get('photo'));
        $cv->setNom($request->get('nom'));
        $cv->setPrenom($request->get('prenom'));
        $cv->setDateNaissance($request->get('date_naissance'));
        $cv->setGender($request->get('gender'));
        $cv->setAdresseEmail($request->get('adresse_email'));
        $cv->setVille($request->get('ville'));
        $cv->setAdresse($request->get('adresse'));
        $cv->setCodePostale($request->get('code_postale'));
        $cv->setTelMobile($request->get('tel_mobile'));

        $em->persist($cv);
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($cv);
        return new JsonResponse($formatted);
    }
    public function updatCvAction($id,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $cv=$em ->getRepository('AlecsoOffreBundle:Cvs')->find($id);
        $cv->setDiplome($request->get('diplome'));
        $cv->setDescription($request->get('description'));
        $cv->setNom($request->get('nom'));
        $cv->setPrenom($request->get('prenom'));
        $cv->setPhoto($request->get('photo'));
        $cv->setDateNaissance($request->get('date_naissance'));
        $cv->setGender($request->get('gender'));
        $cv->setAdresseEmail($request->get('adresse_email'));
        $cv->setVille($request->get('ville'));
        $cv->setAdresse($request->get('adresse'));
        $cv->setCodePostale($request->get('code_postale'));
        $cv->setTelMobile($request->get('tel_mobile.'));
        $em->persist($cv);
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($cv);
        return new JsonResponse($formatted);
    }
    public function alldemandeAction()
    {
        $dmoffre = $this->getDoctrine()->getManager()->getRepository('AlecsoOffreBundle:Dmoffre')->findAll();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($dmoffre);
        return new JsonResponse($formatted);

    }
    public function deletedemandeAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $dmoffre = $em->getRepository('AlecsoOffreBundle:Dmoffre')->find($id);
        $dmoffre->setIdUser(null);
        $em->remove($dmoffre);
        $em->flush();
        $this->addFlash('message','demande supprimé');
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($dmoffre);
        return new JsonResponse($formatted);
    }

    public function adddemandeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $dmoffre = new Dmoffre();
        $dmoffre->setIdCv($request->get('id_cv'));

        $em->persist($dmoffre);
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($dmoffre);
        return new JsonResponse($formatted);
    }
    public function updatdemandeAction($id,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $dmoffre=$em ->getRepository('AlecsoOffreBundle:Dmoffre')->find($id);
        $dmoffre->setIdCv($request->get('id_cv'));

        $em->persist($dmoffre);
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($dmoffre);
        return new JsonResponse($formatted);
    }
    /**
     * List Des Offres
     *
     */
    public function viewAction()
    {
        $offres = $this->getDoctrine()->getRepository('AlecsoOffreBundle:Offre')->findAll();
        return $this->render('@AlecsoOffre/User/view.html.twig',['offres' => $offres]);
    }
    public function viewCvAction()
    {
        $cvs = $this->getDoctrine()->getRepository('AlecsoOffreBundle:Cvs')->findAll();
        return $this->render('@AlecsoOffre/User/viewCv.html.twig',['cvs' => $cvs]);
    }

    public function showOffreAction($id)
    {
        $offres = $this->getDoctrine()->getRepository('AlecsoOffreBundle:Offre')->findAll();
        $em = $this->getDoctrine()->getManager();
        $offre = $em->getRepository('AlecsoOffreBundle:Offre')->find($id);

        if($offre->getIdAdmin() == null){
            $partenaire = $offre->getIdPartenaire();
        }else{
            $partenaire = $em->getRepository('AlecsoOffreBundle:Partenaire')->findOneBy([ 'idUser' => null ]);
        }
        return $this->render('@AlecsoOffre/User/showOffre.html.twig',[
            'offre' => $offre,
            'partenaire' => $partenaire,
            'offres' => $offres,
        ]);
    }
    public function showCvAction($id)
    {
        $cvs = $this->getDoctrine()->getRepository('AlecsoOffreBundle:Cvs')->findAll();
        $em = $this->getDoctrine()->getManager();
        $cv = $em->getRepository('AlecsoOffreBundle:Cvs')->find($id);


        return $this->render('@AlecsoOffre/User/showCv.html.twig',[
            'cvs' => $cv,
            'cvs' => $cvs,
        ]);
    }

    public function demandeMailAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $dmoffres = $em->getRepository('AlecsoOffreBundle:Dmoffre')->find($id);

        return $this->render('@AlecsoOffre/User/demandeMail.html.twig', array(
            'dmoffres' => $dmoffres,
        ));
    }
    public function envoiAction(Request $request)
    {
        $dmoffre = new Dmoffre();
        $form = $this->createForm('OffreBundle', $dmoffre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($dmoffre);
            $em->flush();

            return $this->redirectToRoute('dmoffre_show', array('id' => $dmoffre->getId()));
        }

        return $this->render('@AlecsoOffre/User/demandeMail.html.twig', array(
            'dmoffre' => $dmoffre,
            'form' => $form->createView(),
        ));
    }

    public function ajouteCvAction(Request $request)
    {

        $cv = new Cvs();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $form = $this->createFormBuilder($cv)
            ->add('diplome', TextType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 250
                ),'required' => true))
            ->add('description', TextareaType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 250
                ),'required' => true))
            ->add('photo', FileType::class , array('label' => false,'attr' =>
                array(
                    'accept' => ".jpg,.jpeg,.png"
                )))
            ->add('nom',null ,array('label' => false) )
            ->add('prenom', null,array('label' => false))
            ->add('date_naissance', BirthdayType::class, ['label' => false,
                'placeholder' => [
                    'year' => 'Year', 'month' => 'Month', 'day' => 'Day',
                ]
            ])
            ->add('gender', ChoiceType::class, array('label' => false,
                'choices' => array('Femme' => 'Female', 'Homme' => 'Male'),
                'choices_as_values' => true,
                'expanded' => true,
            ))
            ->add('adresse_email', EmailType::class, array('label' => false,'translation_domain' => 'FOSUserBundle'))
            ->add('ville', ChoiceType::class, [
                'choices'  => [
                    'Algérie' => 'Algérie',
                    'Bahreïn' => 'Bahreïn',
                    'Comores' => 'Comores',

                    'Djibouti' => 'Djibouti',
                    'Égypte' => 'Égypte',
                    'Iraq' => 'Iraq',

                    'Jordanie' => 'Jordanie',
                    'Koweït' => 'Koweït',
                    'Liban' => 'Liban',

                    'Libye' => 'Libye',
                    'Mauritanie' => 'Mauritanie',
                    'Maroc' => 'Maroc',

                    'Oman' => 'Oman',
                    'Palestine' => 'Palestine',
                    'Qatar' => 'Qatar',

                    'Arabie saoudite' => 'Arabie saoudite',
                    'Somalie' => 'Somalie',
                    'Soudan' => 'Soudan',

                    'Syrie' => 'Syrie',
                    'Tunisie' => 'Tunisie',
                    'Émirats arabes unis' => 'Émirats arabes unis',

                    'Yémen' => 'Yémen',
                ],'label' => false, 'attr' =>
                    array(
                        'class' => 'form-control'
                    )
            ])
            ->add('adresse', TextType::class , array('label' => false,'attr' =>
                array(
                    //'maxlength' => 255
                ),'required' => true))
            ->add('code_postale', TextType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 5
                ),'required' => true))
            ->add('tel_mobile', null,array('label' => false))
            ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $file = $request->files->get('form')['photo'];
            $uploads_folder = $this->getParameter('uploads_directory');
            $filename =md5(uniqid()). '.' . $file->guessExtension();
            $file->move(
                $uploads_folder,
                $filename
            );
            $cv->setPhoto($filename);
            $em = $this->getDoctrine()->getManager();
            $User = $em->getRepository('AlecsoOffreBundle:User')->findOneBy([ 'idUser' => $user ]);

            $cv->setIdUser($User);

            $em->persist($cv);
            $em->flush($cv);
            $em->flush($User);
            return $this->redirectToRoute('alecso_cv_viewCv_user');
        }
        return $this->render('@AlecsoOffre/User/ajoutCv.html.twig', array(
            'form' => $form->CreateView(),
            'cv' => $cv
        ));
    }
    public function updateCvAction($id,Request $request)
    {

        $cv = $this->getDoctrine()->getRepository('AlecsoOffreBundle:Cvs')->find($id);
        $img = $cv->getPhoto();
        $cv->setDiplome($cv->getDiplome());
        $cv->setDescription($cv->getDescription());
        $cv->setDateNaissance($cv->getDateNaissance());
        $cv->setVille($cv->getVille());
        $cv->setAdresse($cv->getAdresse());
        $cv->setCodePostale($cv->getCodePostale());
        $cv->setAdresseEmail($cv->getAdresseEmail());
        $cv->setNom($cv->getNom());
        $cv->setPrenom($cv->getPrenom());
        $cv->setGender($cv->getGender());
        $cv->setTelMobile($cv->getTelMobile());
        $form = $this->createFormBuilder($cv)
            ->add('diplome', TextType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 250
                ),'required' => true))
            ->add('nom',null ,array('label' => false) )
            ->add('prenom', null,array('label' => false))
            ->add('gender', ChoiceType::class, array('label' => false,
                'choices' => array('Femme' => 'Female', 'Homme' => 'Male'),
                'choices_as_values' => true,
                'expanded' => true,
            ))
            ->add('date_naissance', BirthdayType::class, ['label' => false,
                'placeholder' => [
                    'year' => 'Year', 'month' => 'Month', 'day' => 'Day',
                ]
            ])
            ->add('description', TextareaType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 255
                ),'required' => true))
            ->add('ville', ChoiceType::class, [
                'choices'  => [
                    'Algérie' => 'Algérie',
                    'Bahreïn' => 'Bahreïn',
                    'Comores' => 'Comores',

                    'Djibouti' => 'Djibouti',
                    'Égypte' => 'Égypte',
                    'Iraq' => 'Iraq',

                    'Jordanie' => 'Jordanie',
                    'Koweït' => 'Koweït',
                    'Liban' => 'Liban',

                    'Libye' => 'Libye',
                    'Mauritanie' => 'Mauritanie',
                    'Maroc' => 'Maroc',

                    'Oman' => 'Oman',
                    'Palestine' => 'Palestine',
                    'Qatar' => 'Qatar',

                    'Arabie saoudite' => 'Arabie saoudite',
                    'Somalie' => 'Somalie',
                    'Soudan' => 'Soudan',

                    'Syrie' => 'Syrie',
                    'Tunisie' => 'Tunisie',
                    'Émirats arabes unis' => 'Émirats arabes unis',

                    'Yémen' => 'Yémen',
                ],'label' => false
            ])
            ->add('adresse', TextType::class , array('label' => false,'attr' =>
                array(
                    //'maxlength' => 255
                ),'required' => true))
            ->add('code_postale', TextType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 8
                ),'required' => true))

            ->add('photo', FileType::class , array('label' => false,'attr' =>
                array(
                    'accept' => ".jpg,.jpeg,.png"
                ), 'required' => false,'data_class' => null))
            ->add('adresse_email', EmailType::class, array('label' => false,'translation_domain' => 'FOSUserBundle'))
            ->add('tel_mobile', null,array('label' => false))
            ->getForm();

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $file = $request->files->get('form')['photo'];
            if($form->get('photo')->getData() == null){
                $cv->setPhoto($img);
            }else{
                $uploads_folder = $this->getParameter('uploads_directory');
                $filename =md5(uniqid()). '.' . $file->guessExtension();
                $file->move(
                    $uploads_folder,
                    $filename
                );
                $cv->setPhoto($filename);
            }
            $em = $this->getDoctrine()->getManager();
            $cv = $em->getRepository('AlecsoOffreBundle:Cvs')->find($id);

            $em->flush();
            return $this->redirectToRoute('alecso_cv_viewCv_user');
        }

        return $this->render('@AlecsoOffre/User/updateCv.html.twig',[
            'form' => $form->CreateView()
        ]);
    }

    /**
     * Delete Un cv .
     *
     */
    public function deleteCVAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $cv = $em->getRepository('AlecsoOffreBundle:Cvs')->find($id);
        $cv->setIdUser(null);
        $em->remove($cv);
        $em->flush();
        $this->addFlash('message','cv supprimé');
        return $this->redirectToRoute('alecso_cv_viewCv_user');
    }
    public function participeAction($id)
    {
        $offres = $this->getDoctrine()->getRepository('AlecsoOffreBundle:Offre')->findAll();
        $em = $this->getDoctrine()->getManager();
        $offre = $em->getRepository('AlecsoOffreBundle:Offre')->find($id);
        $usid = $this->get('security.token_storage')->getToken()->getUser();

        $user = $em->getRepository('AlecsoOffreBundle:User')->find($usid);

        return $this->redirectToRoute('alecso_offre_homepage_user',['offres' => $offres]);
    }

    public function viewParticipeAction()
    {
        $evenements = $this->getDoctrine()->getRepository('AlecsoEvenementBundle:Evenement')->findAll();
        $em = $this->getDoctrine()->getManager();
        $usid = $this->get('security.token_storage')->getToken()->getUser();
        $user = $em->getRepository('AlecsoEvenementBundle:User')->find($usid);
        return $this->render('@AlecsoEvenement/User/viewParticip.html.twig',[
            'user' => $user,
            'events' => $evenements
        ]);
    }

    public function annulePartAction($id)
    {
        $evenements = $this->getDoctrine()->getRepository('AlecsoEvenementBundle:Evenement')->findAll();
        $event = $this->getDoctrine()->getRepository('AlecsoEvenementBundle:Evenement')->find($id);
        $em = $this->getDoctrine()->getManager();
        $usid = $this->get('security.token_storage')->getToken()->getUser();
        $user = $em->getRepository('AlecsoEvenementBundle:User')->find($usid);
        $user->removeIdEvent($event);
        $event->removeIdUser($user);
        $em->flush($user);
        $em->flush($event);
        return $this->redirectToRoute('alecso_evenement_participe_view_user',[
            'user' => $user,
            'events' => $evenements
        ]);
    }


    public function ifparAction($idf)
    {
        $alo = $this->getDoctrine()->getManager()->getRepository("AlecsoOffreBundle:Cvs")->Rechercherifparticipated($idf);

        $serializer = new  Serializer([new ObjectNormalizer()]);

        $formatted = $serializer->normalize($alo);
        return new JsonResponse($formatted);



    }

    public function crudcvAction($diplome,$description,$nom,$prenom,$gender,$mail,$ville,$adresse,$code,$tel,$id)
    {



        $em = $this->getDoctrine()->getManager();

        $m = new Cvs();
        $r = new DateTime();
        $m->setDiplome($diplome);
        $m->setDescription($description);
        $m->setNom($nom);
        $m->setPrenom($prenom);
        $m->setPhoto("test.jpg");
        $m->setGender($gender);
        $m->setAdresseEmail($mail);
        $m->setVille($ville);
        $m->setAdresse($adresse);
        $m->setCodePostale($code);
        $m->setTelMobile($tel);

        $em->persist($m);
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($m);
        return new JsonResponse($formatted);











    }
}