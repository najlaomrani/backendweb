<?php

namespace Alecso\UserBundle\Controller;

use Alecso\EvenementBundle\Entity\Partenaire;
use Alecso\EvenementBundle\Entity\Moderateur;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
        echo 'Hello User';
        return $this->render('@AlecsoUser/Default/index.html.twig');
    }

    public function createPartAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $moderateur = $this->getDoctrine()->getRepository('AlecsoEvenementBundle:Moderateur')->find($user);
        $partenaire = new Partenaire();

        $form = $this->createFormBuilder($partenaire)
            ->add('nom', TextType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 250
                ),'required' => true))
            ->add('type', ChoiceType::class, [
                'choices'  => [
                    'Type 1' => 1,
                    'Type 2' => 2,
                    'Type 3' => 3,
                ],'label' => false,
            ])
            ->add('description', TextareaType::class , array('label' => false,'attr' =>
                array(),'required' => true))
            ->add('tel1', TextType::class , array('label' => false,'attr' =>
                array(),'required' => true))
            ->add('tel2', TextType::class , array('label' => false,'attr' =>
                array(),'required' => false))
            ->add('ville', ChoiceType::class, [
                'choices'  => [
                    'Algérie' => 'Algérie',
                    'Bahreïn' => 'Bahreïn',
                    'Comores' => 'Comores',

                    'Djibouti' => 'Djibouti',
                    'Égypte' => 'Égypte',
                    'Iraq' => 'Iraq',

                    'Jordanie' => 'Jordanie',
                    'Koweït' => 'Koweït',
                    'Liban' => 'Liban',

                    'Libye' => 'Libye',
                    'Mauritanie' => 'Mauritanie',
                    'Maroc' => 'Maroc',

                    'Oman' => 'Oman',
                    'Palestine' => 'Palestine',
                    'Qatar' => 'Qatar',

                    'Arabie saoudite' => 'Arabie saoudite',
                    'Somalie' => 'Somalie',
                    'Soudan' => 'Soudan',

                    'Syrie' => 'Syrie',
                    'Tunisie' => 'Tunisie',
                    'Émirats arabes unis' => 'Émirats arabes unis',

                    'Yémen' => 'Yémen',
                ],'label' => false, 'attr' =>
                    array(
                        'class' => 'form-control'
                    ),'required' => true
            ])
            ->add('adresse', TextType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 150
                ),'required' => true))
            ->add('codepost', TextType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 12
                ),'required' => true))
            ->add('media', FileType::class , array('label' => false,'attr' =>
                array(
                    'accept' => ".jpg,.jpeg,.png"
                )))
            ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $file = $request->files->get('form')['media'];
            $uploads_folder = $this->getParameter('uploads_directory');
            $filename =md5(uniqid()). '.' . $file->guessExtension();
            $file->move(
                $uploads_folder,
                $filename
            );
            $partenaire->setMedia($filename);
            $em = $this->getDoctrine()->getManager();
            $partenaire->setIdUser($moderateur);
            $em->persist($partenaire);
            $em->flush($partenaire);
            return $this->redirectToRoute('fos_user_security_logout');
        }
        return $this->render('@AlecsoUser/Partenaire/create.html.twig', array(
            'formpart' => $form->CreateView()
        ));
    }
}
