<?php

namespace Alecso\EvenementBundle\Controller;

use Alecso\EvenementBundle\Entity\Evenement;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\HttpFoundation\Request;



class EvenementController extends Controller
{
    /**
     * List Des Evenements
     *
     */
    public function viewAction()
    {
        $evenements = $this->getDoctrine()->getRepository('AlecsoEvenementBundle:Evenement')->findAll();
        return $this->render('@AlecsoEvenement/Admin/viewEvent.html.twig',['events' => $evenements]);
    }

    /**
     * Ajoute Un Evenement .
     *
     */
    public function ajouteAction(Request $request)
    {
        $evenenment = new Evenement();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $form = $this->createFormBuilder($evenenment)
            ->add('title', TextType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 250
                ),'required' => true))
            ->add('type', ChoiceType::class, [
                'choices'  => [
                    'Type 1' => 1,
                    'Type 2' => 2,
                    'Type 3' => 3,
                ],'label' => false,
            ])
            ->add('description', TextareaType::class , array('label' => false,'attr' =>
                array(),'required' => true))
            ->add('date_start', DateTimeType::class , array('label' => false,'attr' =>
                array(),'required' => true))
            ->add('date_fin', DateTimeType::class , array('label' => false,'attr' =>
                array(),'required' => true))
            ->add('ville', ChoiceType::class, [
                'choices'  => [
                    'Algérie' => 'Algérie',
                    'Bahreïn' => 'Bahreïn',
                    'Comores' => 'Comores',

                    'Djibouti' => 'Djibouti',
                    'Égypte' => 'Égypte',
                    'Iraq' => 'Iraq',

                    'Jordanie' => 'Jordanie',
                    'Koweït' => 'Koweït',
                    'Liban' => 'Liban',

                    'Libye' => 'Libye',
                    'Mauritanie' => 'Mauritanie',
                    'Maroc' => 'Maroc',

                    'Oman' => 'Oman',
                    'Palestine' => 'Palestine',
                    'Qatar' => 'Qatar',

                    'Arabie saoudite' => 'Arabie saoudite',
                    'Somalie' => 'Somalie',
                    'Soudan' => 'Soudan',

                    'Syrie' => 'Syrie',
                    'Tunisie' => 'Tunisie',
                    'Émirats arabes unis' => 'Émirats arabes unis',

                    'Yémen' => 'Yémen',
                ],'label' => false, 'attr' =>
                    array(
                        'class' => 'form-control'
                    ),'required' => true
            ])
            ->add('adresse', TextType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 150
                ),'required' => true))
            ->add('code_post', TextType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 12
                ),'required' => true))
            ->add('nbr_part', IntegerType::class , array('label' => false,'attr' =>
                array(
                    'min' =>10, 'max' =>1000
                ),'required' => true))
            ->add('media', FileType::class , array('label' => false,'attr' =>
                array(
                    'accept' => ".jpg,.jpeg,.png"
                )))
            ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $file = $request->files->get('form')['media'];
            $uploads_folder = $this->getParameter('uploads_directory');
            $filename =md5(uniqid()). '.' . $file->guessExtension();
            $file->move(
                $uploads_folder,
                $filename
            );
            $evenenment->setMedia($filename);
            $em = $this->getDoctrine()->getManager();

            $admin = $this->getDoctrine()->getRepository('AlecsoEvenementBundle:Admin')->find($user);
            $evenenment->setIdAdmin($admin);
            $em->persist($evenenment);
            $em->flush($evenenment);
            return $this->redirectToRoute('alecso_evenement_view');
        }
        return $this->render('@AlecsoEvenement/Admin/ajouteEvent.html.twig', array(
            'form' => $form->CreateView(),
            'evenement' => $evenenment
        ));
    }

    /**
     * Delete Un Evenement .
     *
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $evenement = $em->getRepository('AlecsoEvenementBundle:Evenement')->find($id);
        $evenement->setIdAdmin(null);

        $em->remove($evenement);
        $em->flush();
        $this->addFlash('message','Hahahahha');
        return $this->redirectToRoute('alecso_evenement_view');
    }

    /**
     * Update Un Evenement .
     *
     */
    public function updateAction($id,Request $request)
    {
        $evenement = $this->getDoctrine()->getRepository('AlecsoEvenementBundle:Evenement')->find($id);
        $img = $evenement->getMedia();
        $evenement->setTitle($evenement->getTitle());
        $evenement->setType($evenement->getType());
        $evenement->setDescription($evenement->getDescription());

        $evenement->setDateStart($evenement->getDateStart());
        $evenement->setDateFin($evenement->getDateFin());

        $evenement->setVille($evenement->getVille());
        $evenement->setAdresse($evenement->getAdresse());
        $evenement->setCodePost($evenement->getCodePost());

        $evenement->setNbrPart($evenement->getNbrPart());


        $form = $this->createFormBuilder($evenement)
            ->add('title', TextType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 250
                ),'required' => true))
            ->add('type', ChoiceType::class, [
                'choices'  => [
                    'Type 1' => 1,
                    'Type 2' => 2,
                    'Type 3' => 3,
                ],'label' => false,
            ])
            ->add('description', TextareaType::class , array('label' => false,'attr' =>
                array(),'required' => true))
            ->add('date_start', DateTimeType::class , array('label' => false,'attr' =>
                array(),'required' => true))
            ->add('date_fin', DateTimeType::class , array('label' => false,'attr' =>
                array(),'required' => true))
            ->add('ville', ChoiceType::class, [
                'choices'  => [
                    'Algérie' => 'Algérie',
                    'Bahreïn' => 'Bahreïn',
                    'Comores' => 'Comores',

                    'Djibouti' => 'Djibouti',
                    'Égypte' => 'Égypte',
                    'Iraq' => 'Iraq',

                    'Jordanie' => 'Jordanie',
                    'Koweït' => 'Koweït',
                    'Liban' => 'Liban',

                    'Libye' => 'Libye',
                    'Mauritanie' => 'Mauritanie',
                    'Maroc' => 'Maroc',

                    'Oman' => 'Oman',
                    'Palestine' => 'Palestine',
                    'Qatar' => 'Qatar',

                    'Arabie saoudite' => 'Arabie saoudite',
                    'Somalie' => 'Somalie',
                    'Soudan' => 'Soudan',

                    'Syrie' => 'Syrie',
                    'Tunisie' => 'Tunisie',
                    'Émirats arabes unis' => 'Émirats arabes unis',

                    'Yémen' => 'Yémen',
                ],'label' => false
            ])
            ->add('adresse', TextType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 150
                ),'required' => true))
            ->add('code_post', TextType::class , array('label' => false,'attr' =>
                array(
                    'maxlength' => 12
                ),'required' => true))
            ->add('nbr_part', IntegerType::class , array('label' => false,'attr' =>
                array(
                    'min' =>10, 'max' =>1000
                ),'required' => true))
            ->add('media', FileType::class , array('label' => false,'attr' =>
                array(
                    'accept' => ".jpg,.jpeg,.png"
                ), 'required' => false,'data_class' => null))
            ->getForm();

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $file = $request->files->get('form')['media'];
            if($form->get('media')->getData() == null){
                $evenement->setMedia($img);
            }else{
                $uploads_folder = $this->getParameter('uploads_directory');
                $filename =md5(uniqid()). '.' . $file->guessExtension();
                $file->move(
                    $uploads_folder,
                    $filename
                );
                $evenement->setMedia($filename);
            }
            $em = $this->getDoctrine()->getManager();
            $evenement = $em->getRepository('AlecsoEvenementBundle:Evenement')->find($id);

            $em->flush();
            return $this->redirectToRoute('alecso_evenement_view');
        }

        return $this->render('@AlecsoEvenement/Admin/editEvent.html.twig',[
            'form' => $form->CreateView()
        ]);
    }

    /**
     * Delete Un Evenement .
     *
     */
    public function statistiqueAction()
    {
        return $this->render('@AlecsoEvenement/Admin/statistique.html.twig');
    }
    /**
     * Show Un Evenement .
     *
     */
    public function showEventAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $evenement = $em->getRepository('AlecsoEvenementBundle:Evenement')->find($id);
        $eventnbr = $evenement->getIdUser()->count();
        if($evenement->getIdAdmin() == null){
            $partenaire = $evenement->getIdPartenaire()->first();
        }else{
            $partenaire = $em->getRepository('AlecsoEvenementBundle:Partenaire')->findOneBy([ 'idUser' => null ]);
        }
        return $this->render('@AlecsoEvenement/Admin/show.html.twig',[
            'event' => $evenement,
            'eventnbr' => $eventnbr,
            'partenaire' => $partenaire
        ]);
    }


    public function statisticAction()
    {
        $em = $this->getDoctrine()->getManager();
        $time1 = new \DateTime();
        $time2 = new \DateTime();
        $time3 = new \DateTime();
        $time4 = new \DateTime();
        $time5 = new \DateTime();
        $time6 = new \DateTime();
        $time7 = new \DateTime();

        $time1->modify('- 6 days');
        $time2->modify('- 5 days');
        $time3->modify('- 4 days');
        $time4->modify('- 3 days');
        $time5->modify('- 2 days');
        $time6->modify('- 1 days');

        $nb1 = $em->getRepository('AlecsoEvenementBundle:Evenement')->getNbWithdare($time1);
        $nb2 = $em->getRepository('AlecsoEvenementBundle:Evenement')->getNbWithdare($time2);
        $nb3 = $em->getRepository('AlecsoEvenementBundle:Evenement')->getNbWithdare($time3);
        $nb4 = $em->getRepository('AlecsoEvenementBundle:Evenement')->getNbWithdare($time4);
        $nb5 = $em->getRepository('AlecsoEvenementBundle:Evenement')->getNbWithdare($time5);
        $nb6 = $em->getRepository('AlecsoEvenementBundle:Evenement')->getNbWithdare($time6);
        $nb7 = $em->getRepository('AlecsoEvenementBundle:Evenement')->getNbWithdare($time7);

        $eventnbradmin = $em->getRepository('AlecsoEvenementBundle:Evenement')->createQueryBuilder('e')->select('COUNT(e)')->where('e.idAdmin IS NOT NULL')->getQuery()->getSingleScalarResult();
        $eventnbrpart  = $em->getRepository('AlecsoEvenementBundle:Evenement')->createQueryBuilder('e')->select('COUNT(e)')->where('e.idAdmin IS NULL')->getQuery()->getSingleScalarResult();
        $evenements = $this->getDoctrine()->getRepository('AlecsoEvenementBundle:Evenement')->findAll();
        $nbrTotal = 0;
        $nbrMaxPart = 0;
        $nbrPart = 0;
        foreach ($evenements as $event){
            echo $event->getTitle();
            $nbrMaxPart += $event->getNbrPart();
            $nbrPart += $event->getIdUser()->Count();
        }

        $nbrTotal =( $nbrPart * 100 )/ $nbrMaxPart;

        return $this->render('@AlecsoEvenement/Admin/statistique.html.twig',[
            'eventnbradmin' => $eventnbradmin,
            'eventnbrpart' => $eventnbrpart,
            'nbrTotal' => $nbrTotal,

            'time1' => $time1,
            'time2' => $time2,
            'time3' => $time3,
            'time4' => $time4,
            'time5' => $time5,
            'time6' => $time6,
            'time7' => $time7,

            'nb1' => (int)$nb1,
            'nb2' => (int)$nb2,
            'nb3' => (int)$nb3,
            'nb4' => (int)$nb4,
            'nb5' => (int)$nb5,
            'nb6' => (int)$nb6,
            'nb7' => (int)$nb7

        ]);
    }
}
