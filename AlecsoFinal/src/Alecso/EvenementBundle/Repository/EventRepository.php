<?php
/**
 * Created by PhpStorm.
 * User: Mars
 * Date: 09/04/2019
 * Time: 16:07
 */

namespace Alecso\EvenementBundle\Repository;

use Doctrine\ORM\EntityRepository;

class EventRepository extends EntityRepository
{

    public function findAll()
    {
        return $this->findBy(array(), array('dateCreate' => 'DESC'));
    }
    public function findByEntitiesByString($str){
        return $this->getEntityManager()
            ->createQuery(
                'SELECT e
                FROM AlecsoEvenementBundle:Evenement e
                WHERE e.title LIKE :str'
            )
            ->setParameter('str', '%'.$str.'%')
            ->getResult();
    }

    public function getNb() {
        return $this->createQueryBuilder('l')
            ->select('COUNT(l)')
            ->getQuery()
            ->getSingleScalarResult();
    }
    public function findRecommended($id){
        return $this->createQueryBuilder('e')
            ->select('e ,COUNT(e) AS HIDDEN counter')
            ->join('e.idUser', 'u')
            ->where('u.id = :id')
            ->groupBy('e.type')
            ->addOrderby('counter', 'DESC')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }

    public function findEventRecommended($id,$type){
        return $this->createQueryBuilder('e')
            ->select('e')
            ->join('e.idUser', 'u')
            ->where('u.id = :id AND e.type = :type')
            ->addOrderby('e.dateCreate', 'DESC')
            ->setParameter('id', $id)
            ->setParameter('type', $type)
            ->getQuery()
            ->getResult();
    }

    public function findEventRecommended2($id){
        return $this->createQueryBuilder('e')
            ->select('e')
            ->leftJoin('e.idUser', 'u')
            ->andWhere('u.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }

    public function getNbWithdare($date) {
        return $this->createQueryBuilder('e')
            ->select('COUNT(e)')
            ->andWhere('e.dateCreate >= :date_start')
            ->andWhere('e.dateCreate <= :date_end')
            ->setParameter('date_start', $date->format('Y-m-d 00:00:00'))
            ->setParameter('date_end', $date->format('Y-m-d 23:59:59'))
            ->getQuery()
            ->getSingleScalarResult();
    }


    /*
    public function findRecommended($id){
        $em = $this->getDoctrine()->getManager();
        $trainings = $em->getRepository('AlecsoEvenementBundle:Evenement')
            ->createQueryBuilder('e');
        $trainings->select('e')
            ->from('AlecsoEvenementBundle:Evenement','e')
            ->join('e.idUser', 'u')
            ->where('u.id = :id')
            ->setParameter('id', $id)
            ->getQuery()->getResult();
         return $trainings;
    }
*/
}